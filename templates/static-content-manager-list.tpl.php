<?php
/**
 * @file
 * Template for the list page.
 */
?>
<ul class="action-links">
  <li>
    <?php print l(t('Add static content'), 'admin/config/static_content_manager/add') ?>
  </li>
</ul>
<?php print drupal_render($filter_form); ?>
<?php print $list; ?>
