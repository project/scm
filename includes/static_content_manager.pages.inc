<?php
/**
 * @file
 * Pages callback for the Static Content Manager module module.
 */

/**
 * Tittle callback for static page.
 */
function static_content_manager_page_title($page) {
  $result = _static_content_manager_get_page($page);
  return $result['title'];
}

/**
 * View callback for static page.
 */
function static_content_manager_page_view($page) {
  $result = _static_content_manager_get_page($page);
  if (empty($result)) {
    drupal_not_found();
    drupal_exit();
  }
  elseif ($result['display_mode'] == 1) {
    return $result['content'];
  }
  elseif ($result['display_mode'] == 2) {
    print $result['content'];
  }
}
