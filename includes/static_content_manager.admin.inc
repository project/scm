<?php

/**
 * @file
 * Admin page callbacks.
 */

/**
 * Callback for the static content list page.
 */
function static_content_manager_admin_list($search_key = NULL) {
  $page = _static_content_manager_get_page(NULL, NULL, $search_key);
  $display_modes = _static_content_manager_get_display_modes();
  $list = _static_content_manager_theme_page($page, $display_modes);

  $filter_form = drupal_get_form('static_content_manager_filter_form', $search_key);

  $page = array(
    '#theme' => 'static_content_manager_list',
    '#list' => $list,
    '#filter_form' => $filter_form,
  );
  return $page;
}

/**
 * Form for adding a new static content page.
 */
function static_content_manager_admin_add_form($form, &$form_state, $static_page = NULL) {
  $form = array();

  $form['static_content_manager_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Static content path'),
    '#description' => t('Specify an alternative path by which this data can be accessed. For example, type "about" when writing an about page. Use a relative path and don\'t add a trailing slash or the URL alias won\'t work.'),
    '#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
    '#required' => TRUE,
  );

  $form['static_content_manager_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Static content title'),
    '#description' => t('Specify title for the static content page'),
    '#required' => TRUE,
  );

  $options = _static_content_manager_get_display_modes();

  $form['static_content_manager_display_mode'] = array(
    '#type' => 'select',
    '#title' => t('Static content display mode'),
    '#options' => $options,
    '#default_value' => 1,
    '#description' => t('Choose display mode for the static content page. If you use theming layer you need to input only the content to be displayed, if you do not use theming layer you need to input all the HTML source for the page.'),
    '#required' => TRUE,
  );

  $form['static_content_manager_body'] = array(
    '#type' => 'text_format',
    '#title' => t('Static content'),
    '#base_type' => 'textarea',
    '#description' => t('Markup for the static page'),
    '#format' => 'full_html',
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Static Page'),
  );

  if (!empty($static_page)) {
    $form['static_content_manager_path']['#default_value'] = $static_page['path'];
    $form['static_content_manager_title']['#default_value'] = $static_page['title'];
    $form['static_content_manager_display_mode']['#default_value'] = $static_page['display_mode'];
    $form['static_content_manager_body']['#default_value'] = $static_page['content'];
    $form['#old_static_page'] = $static_page;

    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete static page'),
      '#submit' => array('static_content_manager_admin_add_form_delete_submit'),
    );
  }

  return $form;
}

/**
 * Form submission handler for the 'Delete' button on the edit form.
 */
function static_content_manager_admin_add_form_delete_submit($form, &$form_state) {
  $form_state['redirect'] = array('admin/config/static_content_manager/' . $form['#old_static_page']['id'] . '/delete');
}

/**
 * Validation hook for static_content_manager_admin_add_form.
 */
function static_content_manager_admin_add_form_validate($form, &$form_state) {
  // We need to validate that the path alias does not already exist.
  if (isset($form['#old_static_page']) && $form['#old_static_page']['path'] == $form_state['values']['static_content_manager_path']) {
    return;
  }
  $alias = $form_state['values']['static_content_manager_path'];
  $has_alias = db_query("SELECT COUNT(alias) FROM {url_alias} WHERE alias = :alias", array(
    ':alias' => $alias,
      ))
      ->fetchField();

  if ($has_alias) {
    form_set_error('static_content_manager_path', t('The path %alias is already in use.', array('%alias' => $alias)));
  }
}

/**
 * Submit hook for static_content_manager_admin_add_form.
 */
function static_content_manager_admin_add_form_submit($form, &$form_state) {
  $static_page = array();
  if (isset($form['#old_static_page'])) {
    $old_static_page = $form['#old_static_page'];
    $static_page = $form['#old_static_page'];
  }
  else {
    $static_page['hash_key'] = _static_content_manager_generate_hash_code(10);
  }
  $static_page['path'] = $form_state['values']['static_content_manager_path'];
  $static_page['title'] = $form_state['values']['static_content_manager_title'];
  $static_page['content'] = $form_state['values']['static_content_manager_body']['value'];
  $static_page['display_mode'] = $form_state['values']['static_content_manager_display_mode'];
  if (isset($static_page['id'])) {
    drupal_write_record('static_content_manager', $static_page, 'id');
    if ($old_static_page['path'] != $static_page['path']) {
      // Delete path.
      path_delete(array('source' => 'static_content_manager/' . $old_static_page['id']));
      $path = array(
        'source' => 'static_content_manager/' . $static_page['id'],
        'alias' => $static_page['path'],
      );
      // Save the path alias.
      path_save($path);
    }
  }
  else {
    drupal_write_record('static_content_manager', $static_page);
    $path = array(
      'source' => 'static_content_manager/' . $static_page['id'],
      'alias' => $static_page['path'],
    );
    // Save the path alias.
    path_save($path);
  }

  // Set massage for user.
  drupal_set_message(t('@type: %title has been @status.', array(
    '@type' => 'Static Page',
    '%title' => $static_page['title'],
    '@status' => isset($old_static_page) ? 'updated' : 'saved',
  )));

  $form_state['redirect'] = 'admin/config/static_content_manager/list';
}

/**
 * Function to build the list page table.
 */
function _static_content_manager_theme_page($page, $display_modes) {
  $header = array(
    t('Title'),
    t('Path'),
    t('Display Mode'),
    array(
      'data' => t('Operations'),
      'colspan' => 3,
    ),
  );

  $rows = array();
  foreach ($page as $single_page) {
    $row = array(
      $single_page->title,
      $single_page->path,
      $display_modes[$single_page->display_mode],
      $single_page->display_mode == 0 ? t('Disabled') : l(t('View'), 'static_content_manager/' . $single_page->id),
      l(t('Edit'), 'admin/config/static_content_manager/' . $single_page->id . '/edit'),
      l(t('Delete'), 'admin/config/static_content_manager/' . $single_page->id . '/delete'),
    );
    $rows[] = $row;
  }

  $output = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(),
    'caption' => '',
    'colgroups' => array(),
    'sticky' => FALSE,
    'empty' => t('No static content available. <a href="@link">Add static content</a>.', array('@link' => url('admin/config/static_content_manager/add'))),
  );

  return theme('table', $output);
}

/**
 * Function to retrieve the available display_modes.
 */
function _static_content_manager_get_display_modes() {
  return array(
    0 => 'Page disabled',
    1 => 'Use theming layer',
    2 => 'Do not use theming layer',
  );
}

/**
 * Form constructor for the static content deletion confirmation form.
 */
function static_content_manager_delete_form($form, &$form_state, $static_page) {
  $static_page = _static_content_manager_get_page($static_page, TRUE);
  if (!empty($static_page)) {
    $form['#static_page'] = $static_page;
    return confirm_form($form, t('Are you sure you want to delete %title?', array('%title' => $static_page['title'])), 'admin/config/static_content_manager/list', t('This action cannot be undone.'), t('Delete'), t('Cancel')
    );
  }
  else {
    drupal_not_found();
    drupal_exit();
  }
}

/**
 * Executes static content deletion.
 */
function static_content_manager_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $page = $form['#static_page'];
    // Delete page.
    _static_content_manager_delete_page($page['id']);
    // Clear cache after deletes.
    cache_clear_all();
    watchdog('static content manager', '@type: deleted %title.', array(
      '@type' => 'Static Page',
      '%title' => $page['title'],
    ));
    drupal_set_message(t('@type: %title has been deleted.', array(
      '@type' => 'Static Page',
      '%title' => $page['title'],
    )));
  }

  $form_state['redirect'] = 'admin/config/static_content_manager/list';
}

/**
 * Implements hook_form().
 *
 * Filter for the content listing.
 */
function static_content_manager_filter_form($form, &$form_state, $search_key) {
  $form = array();

  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter paths'),
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['filter']['search_textfield'] = array(
    '#type' => 'textfield',
    '#default_value' => $search_key,
    '#maxlength' => 128,
    '#size' => 25,
  );
  $form['filter']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
    '#submit' => array('static_content_manager_filter_form_submit_filter'),
  );
  if ($search_key) {
    $form['filter']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#submit' => array('static_content_manager_filter_form_submit_reset'),
    );
  }

  return $form;
}

/**
 * Custom submit function.
 */
function static_content_manager_filter_form_submit_filter($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/static_content_manager/list/' . trim($form_state['values']['search_textfield']);
}

/**
 * Custom submit function.
 */
function static_content_manager_filter_form_submit_reset($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/static_content_manager/list';
}

/**
 * Menu callback.
 *
 * Presents the static page editing form.
 */
function static_content_manager_edit_page($static_page) {
  $static_page = _static_content_manager_get_page($static_page, TRUE);
  if (!empty($static_page)) {
    drupal_set_title(t('<em>Edit @type</em> @title', array(
      '@type' => 'Static Page',
      '@title' => $static_page['title'],
        )), PASS_THROUGH);
    return drupal_get_form('static_content_manager_admin_add_form', $static_page);
  }
  else {
    drupal_not_found();
    drupal_exit();
  }
}

/**
 * Function that generates the hash key.
 */
function _static_content_manager_generate_hash_code($length) {
  $str = '';
  for ($i = 0; $i < $length; $i++) {
    $char = chr(mt_rand(32, 126));
    while (!ctype_alnum($char)) {
      $char = chr(mt_rand(32, 126));
    }
    $str .= $char;
  }

  return $str;
}
