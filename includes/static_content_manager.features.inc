<?php
/**
 * @file
 * Features integration hooks.
 */

/**
 * Implements hook_features_export_options().
 */
function static_content_manager_features_export_options() {
  $options = array();
  $pages = _static_content_manager_get_page();

  foreach ($pages as $page) {
    $options['static_content_manager:' . $page->hash_key] = $page->path;
  }
  return $options;
}

/**
 * Implements hook_features_export().
 */
function static_content_manager_features_export($data, &$export, $module_name) {
  // Module dependencies.
  $export['dependencies']['path'] = 'path';
  $export['dependencies']['static_content_manager'] = 'static_content_manager';

  foreach ($data as $component) {
    $export['features']['static_content_manager'][$component] = $component;
  }
  return array();
}

/**
 * Implements hook_features_export_render().
 */
function static_content_manager_features_export_render($module_name, $data, $export = NULL) {
  $code = array();

  $code[] = '  $static_pages = array();';
  $code[] = '';
  foreach ($data as $page) {
    $page_info = explode(':', $page);
    $hash = $page_info[1];
    $item = _static_content_manager_get_page_by_hash($hash);
    unset($item['id']);
    $code[] = '  $static_pages[] = ' . features_var_export($item, '  ') . ';';
  }

  $code[] = '  return $static_pages;';
  $code = implode("\n", $code);
  return array(
    'static_content_manager_pages' => $code,
  );
}

/**
 * Implements hook_features_rebuild().
 */
function static_content_manager_features_rebuild($module) {
  $items = module_invoke($module, 'static_content_manager_pages');

  foreach ($items as $page) {
    // Delete page in DB on rebuild if it modified.
    $compare = _static_content_manager_get_page_by_hash($page['hash_key']);
    if ($compare) {
      array_shift($compare);
      if ($compare != $page) {
        _static_content_manager_delete_page_by_hash($page['hash_key']);
      }
    }
    $has_alias = db_query("SELECT COUNT(alias) FROM {url_alias} WHERE alias = :alias", array(':alias' => $page['path']))
        ->fetchField();

    // Add page on rebuild.
    if (!$has_alias) {
      drupal_write_record('static_content_manager', $page);
      $path = array(
        'source' => 'static_content_manager/' . $page['id'],
        'alias' => $page['path'],
      );
      // Save the path alias.
      path_save($path);
    }
  }
}

/**
 * Implements hook_features_revert().
 */
function static_content_manager_features_revert($module) {
  static_content_manager_features_rebuild($module);
}

/**
 * Function that deletes static pages by hash.
 */
function _static_content_manager_delete_page_by_hash($hash = NULL) {
  $page = _static_content_manager_get_page_by_hash($hash);
  // Delete page.
  if ($page) {
    _static_content_manager_delete_page($page['id']);
    return TRUE;
  }
  return FALSE;
}

/**
 * Function that retrieves static page info by hash key.
 */
function _static_content_manager_get_page_by_hash($hash = NULL) {
  if (empty($hash)) {
    return FALSE;
  }

  $query = db_select('static_content_manager', 'scm');
  $query->fields('scm');
  $query->condition('scm.hash_key', $hash, '=');
  $result = $query->execute();
  return $result->fetchAssoc();
}
