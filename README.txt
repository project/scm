
Introduction
================

This module is for people who have need of some static pages on they're site.
It provides an easy way to create/manage these pages.

For all bugs, feature requests or support requests please use the
Static Content Manager(SCM) Page issue queue at
http://drupal.org/project/issues/static_content_manager


Key Functionality
=====================

1. Allows you to declare static pages in the administration interface.

2. Allows 3 different display modes:
    1. Page disabled - Declared static page is not accessible.
    2. Use theming layer - Allows you to add static content that will display
          as a standard themed Drupal page.
    3. Do not use theming layer - Allows you to add static content that will be
          displayed on the screen as is. This method is the same as declaring a
          whole HTML page.

4. Provide tools to manage the static content in the administration interface.

5. Provides feature functionality to export the created static content.



Presentation
===============

1. Upload and install the Static Content Manager(SCM) module.

2. Go to Administer -> Config -> Static Content Manager(SCM).

3. Here you have the management interface which is composed of a list of all
    created pages, and a page to create more static content

4. Go to Administer -> Config -> Static Content Manager(SCM) -> List.

5. In the list of all declared static content you can manage the existing
    content: view/edit/delete

5. Go back to Administer -> Config -> Static Content Manager(SCM) -> Add.

6. To add new static content you need to specify a path for the page (one that
    is not used), a title, choose a display mode, and the content itself.
